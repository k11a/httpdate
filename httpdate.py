#!/usr/bin/env python
import urllib2, time
from os import system
from win32api import SetSystemTime
 
def main():
  proxy = urllib2.ProxyHandler({'http': 'http://username:password@proxyurl:proxyport'})
  auth = urllib2.HTTPBasicAuthHandler()
  opener = urllib2.build_opener(proxy, auth, urllib2.HTTPHandler)
  urllib2.install_opener(opener)
  conn = urllib2.urlopen('http://google.ru')
  time.clock()
  t_rtt = time.clock()
  res_time = conn.info().getheader('date')
  t = time.localtime(time.mktime(time.strptime(res_time, '%a, %d %b %Y %H:%M:%S %Z')) - time.timezone)
 
  windows_time_str = time.strftime('%Y-%m-%dT%H:%M:%S', t)
  local_time = time.asctime()
  t_exe = time.clock()
  centi_sec = (t_exe - t_rtt/2)*100
  if centi_sec > 99:
    centi_sec = 99
 
  timetuple = time.strptime(windows_time_str, "%Y-%m-%dT%H:%M:%S")
  win32api.SetSystemTime (timetuple[0], timetuple[1], timetuple[6], timetuple[2], timetuple[3], timetuple[4], timetuple[5], 0)
 
  print 'LOCAL  TIME: ' + local_time
  print 'SERVER TIME: ' + time.asctime(t)
  print 'NEW LOCAL TIME: ' + time.asctime()
  if (t_exe - t_rtt/2) >= 1:
    print 'Round trip time is too long. Time error might be larger than 1 sec.'
 
if __name__ == '__main__':
  main()
